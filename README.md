# Jackdaw SSH API

Terraform Module for interacting with the Jackdaw SSH API. In order to use it you'll need to have access to the Jackdaw DNS SSH API configured. 

> :zap: This module only creates or updates CNAMES. See below for more info.

This module conforms to the [terraform standard module
structure](https://www.terraform.io/docs/modules/create.html#standard-module-structure).

Configuration variables are documented in [variables.tf](variables.tf).

## Usage Example

The [examples](examples/) directory contains example of use. A basic usage
example is available in [examples/main.tf](examples/main.tf).

## Limitations

So far this module will `create` or `update` CNAME records only. It will not perform `delete` or `get` operations.

It does not get hostname information: `get nameinfo $hostname`.

It does not perform **CRUD** operations on `SSHFP` records.
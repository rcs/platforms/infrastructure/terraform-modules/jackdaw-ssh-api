data "external" "jackdaw_ssh_api" {
  program = ["bash", "${path.module}/scripts/api.sh"]

  query = {
    username = var.jackdaw_username
    key      = var.jackdaw_key
    domain   = var.alias
    ip       = var.ip
  }
}

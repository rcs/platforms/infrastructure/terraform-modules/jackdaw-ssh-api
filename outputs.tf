output "action" {
  value = data.external.jackdaw_ssh_api.result.action
}
output "domain" {
  value = data.external.jackdaw_ssh_api.result.domain
}
output "target-host" {
  value = data.external.jackdaw_ssh_api.result.target
}


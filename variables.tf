variable "jackdaw_username" {
  default     = ""
  type        = string
  description = "user to connect to jackdaw SSH API as"
}

variable "jackdaw_key" {
  default     = ""
  type        = string
  description = "Private Key to use when connecting"
}

variable "alias" {
  default     = ""
  type        = string
  description = "The alias to be created"
}

variable "ip" {
  default     = ""
  type        = string
  description = "IP to point the alias at. A reverse IP lookup will be done on this IP"
}



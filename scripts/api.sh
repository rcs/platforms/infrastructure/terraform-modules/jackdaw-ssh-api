#!/usr/bin/env bash

set -e
# Want to run this script without terraform?
# echo "{\"username\": \"mwsuser\", \"key\": \"path/to/key\", \"ip\": \"ip\", \"domain\": \"something.hpc.cam.ac.uk\"}" | ./api.sh

function check_deps() {
  if [ ! -f $(which jq) ]
  then
    echo "jq not installed!"
    exit 1
  fi
}

# Do horrible things to the input
function parse_input() {
  eval "$(jq -r '@sh "export KEY=\(.key) USER_NAME=\(.username) IP=\(.ip) DOMAIN=\(.domain)"')"
}

# Get PTR record only
function parse_ip() {
  TARGET=$(dig @131.111.8.42 +short -x $IP | grep -v in-addr)
  # Remove trailing dot
  TARGET=${TARGET%\.}
}

# List of return codes and errors for put command:
# 1 - Invocation error
# 2 - Database unavailable
# 3 - No valid domain for hostname
# 5 - Target not in a permitted domain
# 7 - Hostname in a delagated domain
# 8 - Unknown user
# 99 - Database error (probably constraint violation)
# 100 - Other error

function check_record() {
  RESULTS=$(ssh -i $KEY $USER_NAME@ssh.jackdaw.cam.ac.uk ipreg_api PUT CNAME $DOMAIN $TARGET)

  RETURNCODE=$(echo $RESULTS | jq -r .status)
  if [ $RETURNCODE -ne "0" ]
  then
    echo $(echo $RESULTS | jq '.message')
    return $RETURNCODE
  fi

  # Safely produce a JSON object containing the result value.
  # jq will ensure that the value is properly quoted
  # and escaped to produce a valid JSON string.
  FORMAT=$(echo $RESULTS | jq -r '{status: .status|tostring, message: .message, target: .target, domain: .domain, action: .action}')
  echo $FORMAT
}

check_deps
parse_input
parse_ip
check_record
